class SessionsController < ApplicationController

   skip_before_action :require_login, only: [:create, :new]

   # The login form
   def new
      @user = User.new
   end

   def create
      user = User.authenticate(params[:email], params[:password])
      if user
         session[:user_id] = user.id
         user.update(:api_token => SecureRandom.hex) unless user.api_token
         # redirect_to root_url
         redirect_to properties_url
      else
         render "new"
      end
   end

   def destroy
   end
end
