class TicketsController < ApplicationController

   def index
      @property = Property.find(params[:property_id])
      @tickets = @property.tickets
   end

   def new
      @property = Property.find(params[:property_id])
      @ticket = Ticket.new
   end

   def create
   end

   def show
      @property = Property.find(params[:property_id])
      @ticket = Ticket.find(params[:id])
   end

end
