class ApplicationController < ActionController::Base
   before_action :require_login

   helper_method :current_user
   helper_method :require_admin
   helper_method :require_login

   def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
   rescue ActiveRecord::RecordNotFound
      flash[:alert] = "You have been logged out."
      @current_user = nil
      session[:user_id] = nil
   end

   def require_admin
      if !current_user.is_admin
         redirect_to login_url, :notice => "Admin Status Required"
      end
   end

   def require_login
      if !current_user
         redirect_to login_url, :notice => "Login Required"
      end
   end

end
