class PropertiesController < ApplicationController

   def index
      @memberships = current_user.memberships
   end

   def show
      @property = Property.find(params[:id])
   end

end
