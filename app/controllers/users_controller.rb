class UsersController < ApplicationController
   helper_method :can_edit
   skip_before_action :require_login, only: [:create, :new]

   def new
      @user = User.new
   end
   def create
      @user = User.new(user_params)
      if @user.save
         session[:user_id] = @user.id
         redirect_to root_path
      else
         render "new"
      end
   end

   def index
      require_admin
     @users = User.page(params[:page]).order("created_at DESC").all
   end

   def show
      @user = User.find(params[:id])
   end

   def edit
      self_edit_only
      @user = User.find(params[:id])
   end

   # update a user object
   def update
      self_edit_only
      @user = User.find(params[:id])
      if @user.update(update_params)
         redirect_to user_url, :notice => "Update successful!"
      else
         render "edit"
      end
   end

   # Delete a user
   def destroy
      self_edit_only
      @user = User.find(params[:id])
      if @user.destroy
         redirect_to root_path, :notice => "Account deleted!"
      else
         render "edit", :notice => "Deletion failure!"
      end
   end

   private

   def can_edit
      return current_user && (current_user.id == Integer(params[:id]) || current_user.is_admin)
   end

   # Only the same user and admins can edit a user's profile
   def self_edit_only
      #if current_user.id != Integer(params[:id]) && !current_user.is_admin
      if !can_edit
         redirect_to user_url, :notice => "You don't have permission to do that."
      else
      end
   end

   def user_params
      params.require(:user).permit(:email, :username, :password, :password_confirmation)
   end

   def update_params
      params.require(:user).permit(:email, :username, :avatar, :bio, :first_name, :last_name)
   end
end
