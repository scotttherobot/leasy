class MembershipsController < ApplicationController

   def index
      @property = Property.find(params[:property_id])
      @members = @property.memberships
   end

end
