class Property < ActiveRecord::Base
  belongs_to :user

  has_many :property_events, :dependent => :destroy
  has_many :tickets, :dependent => :destroy
  has_many :memberships, :dependent => :destroy

   # Requires paperclip gem for avatars
   has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "100x100#", :large => "500x500#" }, :default_url => ":style/missing_house.jpg"
   validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  after_save :log_event

   private

   def log_event
      PropertyEvent.new(:property => self,
                        :user => self.user,
                        :event_type => "property",
                        :text => "created property " + self.street_address).save
   end

end
