class User < ActiveRecord::Base
   attr_accessor :password
   before_save :encrypt_password
   after_initialize :init

   validates_confirmation_of :password
   validates_presence_of :password, :on => :create
   validates_presence_of :email
   validates_uniqueness_of :email

   has_many :properties
   has_many :property_events
   has_many :tickets
   has_many :memberships, :dependent => :destroy

   # Requires paperclip gem for avatars
   has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "100x100#", :large => "500x500#" }, :default_url => "/images/:style/missing.png"
   validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

   def init
      self.role ||= 1
   end

   def self.authenticate(email, password)
      user = find_by_email(email)

      if user && BCrypt::Password.new(user.password_hash) == password
         user
      else
         nil
      end
   end

   def encrypt_password
      if password.present?
         self.password_hash = BCrypt::Password.create(password)
      end
   end

   def is_admin
      return self.role == 2
   end

   def make_admin
      self.role = 2
   end
end
