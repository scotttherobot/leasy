class Membership < ActiveRecord::Base
  belongs_to :property
  belongs_to :user

  after_save :log_event

  private
  
  def log_event
      PropertyEvent.new(:property => self.property,
                        :user => self.user,
                        :event_type => "membership",
                        :text => self.user.username + " joined the property").save
  end

end
