class Ticket < ActiveRecord::Base
  belongs_to :property
  belongs_to :user

  after_save :log_event

   # Requires paperclip gem for avatars
   has_attached_file :photo1, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
   validates_attachment_content_type :photo1, :content_type => /\Aimage\/.*\Z/

   has_attached_file :photo2, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
   validates_attachment_content_type :photo2, :content_type => /\Aimage\/.*\Z/


   def log_event
      PropertyEvent.new(:property => self.property,
                        :user => self.user,
                        :event_type => "ticket",
                        :text => "opened ticket " + self.title).save
   end

end
