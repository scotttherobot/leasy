class CreateUsers < ActiveRecord::Migration
  def change
     create_table "users" do |t|
       t.string   "username"
       t.string   "email"
       t.string   "password_hash"
       t.integer  "role"
       t.string   "api_token"
       t.datetime "api_token_expire"
       t.string   "first_name"
       t.string   "last_name"
       t.string   "bio"
       t.datetime "created_at"
       t.datetime "updated_at"
       t.string   "avatar_file_name"
       t.string   "avatar_content_type"
       t.integer  "avatar_file_size"
       t.datetime "avatar_updated_at"
     end
  end
end
