class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.references :property, index: true
      t.references :user, index: true
      t.string :role

      t.timestamps
    end
  end
end
