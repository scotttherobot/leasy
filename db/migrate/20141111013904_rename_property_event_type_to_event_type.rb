class RenamePropertyEventTypeToEventType < ActiveRecord::Migration
  def change
     change_table :property_events do |t|
        t.rename :type, :event_type
     end
  end
end
