class AddAttachmentPhoto2ToTickets < ActiveRecord::Migration
  def self.up
    change_table :tickets do |t|
      t.attachment :photo2
    end
  end

  def self.down
    remove_attachment :tickets, :photo2
  end
end
