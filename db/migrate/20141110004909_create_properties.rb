class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.references :user, index: true
      t.string :street_address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :description

      t.timestamps
    end
  end
end
