class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.references :property, index: true
      t.references :user, index: true
      t.string :title
      t.string :location
      t.string :description
      t.string :attempted

      t.timestamps
    end
  end
end
