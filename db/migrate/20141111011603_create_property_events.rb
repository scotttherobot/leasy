class CreatePropertyEvents < ActiveRecord::Migration
  def change
    create_table :property_events do |t|
      t.references :property, index: true
      t.references :user, index: true
      t.string :type
      t.string :text

      t.timestamps
    end
  end
end
