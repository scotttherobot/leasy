class AddAttachmentPhoto1ToTickets < ActiveRecord::Migration
  def self.up
    change_table :tickets do |t|
      t.attachment :photo1
    end
  end

  def self.down
    remove_attachment :tickets, :photo1
  end
end
